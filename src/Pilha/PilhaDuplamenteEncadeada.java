package Pilha;

import Conta.Conta;
import Conta.ContaInvalidaException;

/**
 * 
 * Fila encadada deve ser utilizada quando for necessario trabalhar com listas
 * que o seu funcionamento deve ser LIFO(primeiro a entrar, ultimo a sair)
 * 
 * @author Haniel Barros
 * @version 1.0.0
 *
 */

public class PilhaDuplamenteEncadeada {

	private Conta primeiraConta;
	private int inseridos;
	
	/**
	 * Esse metodo serve para inserior um novo objeto no comeco da pilha
	 * @param c
	 * @throws ContaInvalidaException
	 */

	public void push(Conta c) throws ContaInvalidaException {
		if (c == null)
			throw new ContaInvalidaException();
		if (primeiraConta == null) {
			primeiraConta = c;
			inseridos++;
		} else {
			Conta aux = primeiraConta;

			while (aux.getProximo() != null) {
				aux = aux.getProximo();
			}

			aux.setProximo(c);
			inseridos++;
		}

	}
	
	/**
	 * Esse metodo serve para remover o primeiro elemento da pilha
	 * @return
	 */

	public Conta pop() {

		Conta auxRetorno = null;
		Conta aux = primeiraConta;
		Conta auxAnterior = primeiraConta;

		if (primeiraConta.getProximo() == null) {
			primeiraConta = null;
		}

		while (aux.getProximo() != null) {
			auxAnterior = aux;
			aux = aux.getProximo();
		}

		auxRetorno = aux;
		auxAnterior.setProximo(null);
		inseridos--;

		return auxRetorno;

	}
	
	/**
	 * Esse metodo serve para verificar o primeiro elemento da pilha, se existente
	 * @return
	 */

	public Conta top() {
		if (primeiraConta.getProximo() == null) {
			return primeiraConta;
		}

		Conta aux = primeiraConta;

		while (aux.getProximo() != null) {
			aux = aux.getProximo();
		}

		return aux;
	}
	
	/**
	 * Esse metodo serve para saber o tamanho da pilha
	 * @return
	 */

	public int size() {
		return inseridos;
	}
	
	/**
	 * Esse metodo serve para saber se a pilha esta vazia
	 * @return
	 */

	public boolean isEmpty() {
		return inseridos == 0;
	}

}
