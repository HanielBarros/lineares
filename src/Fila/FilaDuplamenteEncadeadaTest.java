package Fila;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Conta.Conta;
import Conta.ContaInvalidaException;

class FilaDuplamenteEncadeadaTest {
	
	private FilaDuplamenteEncadeada fila;
	

	private Conta c1 = new Conta(1, 2, "Haniel");
	private Conta c2 = new Conta(2, 2, "LTI");
	private Conta c3 = new Conta(3, 3, "Abella");

	@BeforeEach
	void setUp() throws Exception {
		fila = new FilaDuplamenteEncadeada();
		
	}

	@Test
	void enqueueTest() throws ContaInvalidaException {
		fila.enqueue(c1);
		fila.enqueue(c2);
		fila.enqueue(c3);
		
		assertEquals(3, fila.size());
	}
	
	@Test
	void enqueueAndFrontTest() throws ContaInvalidaException {
		fila.enqueue(c3);
		fila.enqueue(c1);
		fila.enqueue(c2);
		
		assertEquals(c3, fila.front());
	}
	
	@Test
	void enqueueAndDequeueAndFrontTest() throws ContaInvalidaException {
		fila.enqueue(c3);
		fila.enqueue(c1);
		fila.enqueue(c2);
		
		fila.dequeue();
		
		assertEquals(c1, fila.front());
	}
	
	@Test
	void enqueueNullObjectTest() throws ContaInvalidaException {
		Assertions.assertThrows(ContaInvalidaException.class, () -> {
			fila.enqueue(null);
		});
	}
	
	@Test
	void dequeueTest() throws ContaInvalidaException {
		fila.enqueue(c1);
		fila.enqueue(c2);
		fila.enqueue(c3);
		
		fila.dequeue();
		
		assertEquals(2, fila.size());
	}
	
	@Test
	void frontTest() throws ContaInvalidaException {
		fila.enqueue(c2);
		fila.enqueue(c1);
		fila.enqueue(c3);
		
		assertEquals(c2, fila.front());
	}
	
	@Test
	void sizeTest() throws ContaInvalidaException {
		fila.enqueue(c1);
		assertEquals(1, fila.size());
		fila.enqueue(c2);
		assertEquals(2, fila.size());
	}

}
