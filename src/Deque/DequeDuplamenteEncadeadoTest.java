package Deque;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Conta.Conta;
import Conta.ContaInvalidaException;
import Conta.PosicaoInvalidaException;

class DequeDuplamenteEncadeadoTest {

	private DequeDuplamenteEncadeado dq;

	private Conta c1 = new Conta(1, 2, "Haniel");
	private Conta c2 = new Conta(2, 2, "LTI");
	private Conta c3 = new Conta(3, 3, "Abella");

	@BeforeEach
	void setUp() throws Exception {
		dq = new DequeDuplamenteEncadeado();
	}

	@Test
	void insertFirstObjectTest() throws ContaInvalidaException {
		dq.insertFirst(c1);
		dq.insertFirst(c2);

		assertEquals(2, dq.size());
	}

	@Test
	void insertLastObjectTest() throws ContaInvalidaException {
		dq.insertLast(c2);
		dq.insertLast(c1);

		assertEquals(2, dq.size());
	}
	
	@Test
	void getFirstObjectTest() throws ContaInvalidaException {
		dq.insertFirst(c2);
		dq.insertLast(c1);
		
		assertEquals(c2, dq.getFirst());
	}
	
	@Test
	void getLastObjectTest() throws ContaInvalidaException {
		dq.insertFirst(c1);
		dq.insertFirst(c2);
		
		assertEquals(c1, dq.getLast());
	}

	@Test
	void insertNullObjectTest() throws ContaInvalidaException {
		Assertions.assertThrows(ContaInvalidaException.class, () -> {
			dq.insertFirst(null);
		});
	}

	@Test
	void removeObjectByValueTest() throws ContaInvalidaException {
		dq.insertFirst(c1);
		dq.insertFirst(c2);
		dq.insertFirst(c3);

		dq.removeByValue(c3);

		assertEquals(2, dq.size());

	}
	
	@Test
	void removeNullObjectTest() throws ContaInvalidaException {
		Assertions.assertThrows(ContaInvalidaException.class, () -> {
			dq.removeByValue(null);
		});

	}
	
	@Test
	void removeObjectByIndexTest() throws ContaInvalidaException, PosicaoInvalidaException {
		dq.insertFirst(c1);
		dq.insertFirst(c2);
		dq.insertFirst(c3);
		
		dq.removeByIndex(2);
		
		assertEquals(2, dq.size());
	}
	
	@Test
	void removeInvalidIndexTest() throws PosicaoInvalidaException {
		Assertions.assertThrows(PosicaoInvalidaException.class, () -> {
			dq.removeByIndex(-1);
		});

	}
	
	@Test
	void removeLastPositionTest( ) throws ContaInvalidaException {
		dq.insertFirst(c1);
		dq.insertFirst(c2);
		dq.insertFirst(c3);
		
		dq.removeLast();
		
		assertEquals(c2, dq.getLast());
		
	}
	
	@Test
	void removeFirstPositionTest( ) throws ContaInvalidaException {
		dq.insertFirst(c1);
		dq.insertLast(c2);
		
		dq.removeFirst();
		
		assertEquals(c2, dq.getLast());
		
	}

	@Test
	void sizeTest() throws ContaInvalidaException {
		dq.insertFirst(c1);
		dq.insertLast(c3);
		dq.insertFirst(c2);

		assertEquals(3, dq.size());

	}

}
