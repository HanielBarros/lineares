package Lista;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Conta.Conta;
import Conta.ContaInvalidaException;
import Conta.PosicaoInvalidaException;

class ListaDuplamenteEncadeadaTest {
	
	private ListaDuplamenteEncadeada lst;
	
	private Conta c1 = new Conta(1, 2, "Haniel");
	private Conta c2 = new Conta(2, 2, "LTI");
	private Conta c3 = new Conta(3, 3, "Abella");

	@BeforeEach
	void setUp() throws Exception {
		lst = new ListaDuplamenteEncadeada();
		
	}

	@Test
	void validSizeTest() throws ContaInvalidaException, PosicaoInvalidaException {
		lst.add(c1);
		lst.add(c2);
		lst.add(c3);
		
		assertEquals(3, lst.size());
	}
	
	@Test
	void validContaTest() throws ContaInvalidaException, PosicaoInvalidaException {
		lst.add(c1);
		lst.add(c2);
		lst.add(c3);
		
		assertEquals(c2, lst.get(1));
	}
	
	@Test
	void addNotValidElementTest() {
		Assertions.assertThrows(ContaInvalidaException.class, () -> {
			lst.add(null);
		});
	}
	
	@Test
	void getValidElementTest() throws ContaInvalidaException, PosicaoInvalidaException {
		lst.add(c1);
		lst.add(c2);
		lst.add(c3);
		
		assertEquals(c2, lst.get(1));
	}
	
	@Test
	void getNotValidElementTest() {
		Assertions.assertThrows(PosicaoInvalidaException.class, () -> {
			lst.get(-1);
		});
	}
	
	@Test
	void removeValidElementByPosTest() throws ContaInvalidaException, PosicaoInvalidaException {
		lst.add(c1);
		lst.add(c2);
		lst.add(c3);
		
		lst.remove(0);
		
		assertEquals(c2, lst.get(0));
	}
	
	@Test
	void removeValidElementTest() throws ContaInvalidaException, PosicaoInvalidaException {
		lst.add(c1);
		lst.add(c2);
		lst.add(c3);
		
		lst.remove(c2);
		
		assertEquals(c3, lst.get(1));
	}
	
	@Test
	void removeValidElementAndGetSizeTest() throws ContaInvalidaException, PosicaoInvalidaException {
		lst.add(c1);
		lst.add(c2);
		lst.add(c3);
		
		lst.remove(c2);
		
		assertEquals(2, lst.size());
	}
	
	@Test
	void removeInvalidElementByPosTest() throws PosicaoInvalidaException {
		Assertions.assertThrows(PosicaoInvalidaException.class, () -> {
			lst.remove(-1);
		});
	}
	
	@Test
	void removeInvalidElementTest() throws ContaInvalidaException {
		Assertions.assertThrows(ContaInvalidaException.class, () -> {
			lst.remove(null);
		});
	}
	

}
