package Lista;

import Conta.Conta;
import Conta.ContaInvalidaException;
import Conta.PosicaoInvalidaException;

/**
 * Lista Duplamente deve ser utilizada quando necessita trabalhar
 * com listas que o seu funcionamento deve percorrer o inicio da lista
 * ou o final da lista, para melhor eficiencia
 * 
 * @author Haniel Barros
 * @version 1.0.0
 */


public class ListaDuplamenteEncadeada {

	private Conta primeiraConta;
	private Conta ultimaConta;

	private int inseridos;
	
	/**
	 * Adiciona o objeto na lista encadeada
	 * @param c
	 * @throws ContaInvalidaException
	 */

	public void add(Conta c) throws ContaInvalidaException {
		if (c == null) {
			throw new ContaInvalidaException();
		}

		if (primeiraConta == null) {
			primeiraConta = c;
			ultimaConta = c;
		} else {
			ultimaConta.setProximo(c);
			ultimaConta = c;
		}

		inseridos++;

	}
	
	/**
	 * Pega o objeto pela posicao que ele esta inserido na lista
	 * @param pos
	 * @return
	 * @throws PosicaoInvalidaException
	 */

	public Conta get(int pos) throws PosicaoInvalidaException {
		if (pos < 0 || pos > size()) {
			throw new PosicaoInvalidaException();
		}

		Conta aux = primeiraConta;

		if (pos == 0) {
			return primeiraConta;
		}

		for (int i = 0; i < inseridos; i++) {
			if (i == pos) {
				return aux;
			} else {
				aux = aux.getProximo();
			}

		}
		return aux;

	}
	
	/**
	 * Remove o objeto, se existente, na lista atual
	 * @param c
	 * @throws ContaInvalidaException
	 */

	public void remove(Conta c) throws ContaInvalidaException {
		if (c == null) {
			throw new ContaInvalidaException();
		}

		if (c.equals(primeiraConta)) {
			primeiraConta = primeiraConta.getProximo();
		} else {
			Conta aux = primeiraConta;
			while (aux.getProximo() != c) {
				aux = aux.getProximo();
			}
			aux.setProximo(c.getProximo());
		}

		inseridos--;

	}
	
	/**
	 * Se existente, remove o objeto pela posicao atual na lista
	 * @param pos
	 * @throws PosicaoInvalidaException
	 * @throws ContaInvalidaException
	 */

	public void remove(int pos) throws PosicaoInvalidaException, ContaInvalidaException {
		if (pos < 0 || pos > size()) {
			throw new PosicaoInvalidaException();
		}

		Conta aux = primeiraConta;

		if (pos == 0) {
			primeiraConta = primeiraConta.getProximo();
			inseridos--;
		} else {
			for (int i = 0; i < size(); i++) {
				if (i == pos) {
					remove(aux);
				} else {
					aux = aux.getProximo();
				}
			}
		}

	}
	/**
	 * Metodo para saber se a lista esta vazia ou nao
	 * @return
	 */

	public boolean isEmpty() {
		return inseridos == 0;
	}
	
	/**
	 * Metodo para saber o tamanho da lista
	 * @return
	 */

	public int size() {
		return inseridos;
	}

}
