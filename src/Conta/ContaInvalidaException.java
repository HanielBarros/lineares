package Conta;



public class ContaInvalidaException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContaInvalidaException() {
		super("Conta Invalida");
	}

}
