package Pilha;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Conta.Conta;
import Conta.ContaInvalidaException;

class PilhaDuplamenteEncadeadaTest {
	
	PilhaDuplamenteEncadeada pilha;
	
	private Conta c1 = new Conta(1, 2, "Haniel");
	private Conta c2 = new Conta(2, 2, "LTI");
	private Conta c3 = new Conta(3, 3, "Abella");

	@BeforeEach
	void setUp() throws Exception {
		pilha = new PilhaDuplamenteEncadeada();
	}

	@Test
	void pushTest() throws ContaInvalidaException {
		pilha.push(c1);
		pilha.push(c2);
		pilha.push(c3);
		
		assertEquals(3, pilha.size());
	}
	
	@Test
	void pushNullObjectTest() throws ContaInvalidaException {
		Assertions.assertThrows(ContaInvalidaException.class, () -> {
			pilha.push(null);
		});
	}
	
	@Test
	void popTest() throws ContaInvalidaException {
		pilha.push(c1);
		pilha.push(c2);
		pilha.push(c3);
		
		pilha.pop();
		
		assertEquals(c2, pilha.top());
	}
	
	@Test
	void popStackEmptyTest() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			pilha.pop();
		});
		
	}
	
	@Test
	void popSizeTest() throws ContaInvalidaException {
		pilha.push(c1);
		pilha.push(c2);
		pilha.push(c3);
		
		pilha.pop();
		
		assertEquals(2, pilha.size());
	}
	
	@Test
	void topTest() throws ContaInvalidaException {
		pilha.push(c1);
		pilha.push(c2);
		pilha.push(c3);
		
		assertEquals(c3, pilha.top());
	}
	

}
