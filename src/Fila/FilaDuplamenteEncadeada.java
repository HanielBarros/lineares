package Fila;

import Conta.Conta;
import Conta.ContaInvalidaException;

/**
 * 
 * Fila encadada deve ser utilizada quando for necessario trabalhar com listas
 * que o seu funcionamento deve ser FIFO(primeiro a entrar, primeiro a sair)
 * 
 * @author Haniel Barros
 * @version 1.0.0
 *
 */

public class FilaDuplamenteEncadeada {

	private Conta primeiraConta;
	private int inseridos;

	/**
	 * Esse metodo serve para colocar o objeto na primeira posicao da fila
	 * 
	 * @param c
	 * @throws ContaInvalidaException
	 */

	public void enqueue(Conta c) throws ContaInvalidaException {
		if (c == null) {
			throw new ContaInvalidaException();	
		}

		if (primeiraConta == null) {
			primeiraConta = c;
			inseridos++;
		} else {
			Conta aux = primeiraConta;

			while (aux.getProximo() != null) {
				aux = aux.getProximo();
			}
			aux.setProximo(c);
			inseridos++;
		}
	}
	
	/**
	 * Esse metodo serve para remover o objeto da fila
	 * @return
	 */

	public Conta dequeue() {
		Conta aux = primeiraConta;

		if (primeiraConta != null) {
			primeiraConta = primeiraConta.getProximo();
			inseridos--;
		}
		return aux;
	}

	/**
	 * Esse metodo retorna o primeiro objeto da fila
	 * 
	 * @return
	 */

	public Conta front() {
		return primeiraConta;
	}

	/**
	 * Esse metodo serve para saber o tamanho da fila
	 * 
	 * @return
	 */
	
	public int size() {
		return inseridos;
	}
	
	/**
	 * Esse metodo serve para saber se a fila esta vazia ou nao
	 * @return
	 */

	public boolean isEmpyt() {
		return inseridos == 0;
	}

}
