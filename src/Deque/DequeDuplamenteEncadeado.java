package Deque;

import Conta.Conta;
import Conta.ContaInvalidaException;
import Conta.PosicaoInvalidaException;

/**
 * 
 * DequeEncadeado deve ser utilizada quando necessita trabalhar
 * com listas que o seu funcionamento deve ser inserir no inicio e no fim 
 * 
 * 
 * @author Haniel Barros
 * @version 1.0.0
 */

public class DequeDuplamenteEncadeado {

	private Conta primeiraConta;
	private int inseridos;
	

	/**
	 * Esse metodo ira inserir o objeto na primeira posicao
	 * 
	 * @param c
	 * @throws ContaInvalidaException
	 */

	public void insertFirst(Conta c) throws ContaInvalidaException {

		if (c == null) {
			throw new ContaInvalidaException();
		}

		if (primeiraConta == null) {
			this.primeiraConta = c;
			inseridos++;
		} else {
			c.setProximo(primeiraConta);
			primeiraConta = c;
			inseridos++;
		}

	}

	/**
	 * Esse metodo ira inserir o metodo na ultima posicao
	 * 
	 * @param c
	 * @throws ContaInvalidaException
	 */

	public void insertLast(Conta c) throws ContaInvalidaException {

		if (c == null) {
			throw new ContaInvalidaException();
		}

		if (primeiraConta == null) {
			this.primeiraConta = c;
			inseridos++;
		} else {

			Conta aux = primeiraConta;

			while (aux.getProximo() != null) {
				aux = aux.getProximo();
			}
			aux.setProximo(c);
			inseridos++;

		}

	}

	/**
	 * Esse metodo ira pegar a primeira posicao do deque encadeado
	 * 
	 * @return
	 */

	public Conta getFirst() {
		return primeiraConta;
	}

	/**
	 * Esse metodo ira pegar a ultima posicao do deque encadeado
	 * 
	 * @return
	 */

	public Conta getLast() {

		Conta aux = primeiraConta;

		while (aux != null && aux.getProximo() != null) {
			aux = aux.getProximo();
		}

		return aux;

	}

	/**
	 * Esse metodo ira remover a primeira posicao do deque encadeado
	 * 
	 * @return
	 */

	public Conta removeFirst() {

		Conta aux = primeiraConta;

		if (primeiraConta != null) {
			primeiraConta = primeiraConta.getProximo();
			inseridos--;
		}

		return aux;

	}

	/**
	 * Esse metodo ira remover um objeto por referencia
	 * 
	 * @param c
	 * @throws ContaInvalidaException
	 */

	public void removeByValue(Conta c) throws ContaInvalidaException {

		if (c == null) {
			throw new ContaInvalidaException();
		}

		if (c != null) {
			if (c.equals(primeiraConta)) {
				primeiraConta = primeiraConta.getProximo();
				inseridos--;
			} else {
				Conta aux = primeiraConta;

				while (aux.getProximo() != null && !aux.getProximo().equals(c)) {
					aux = aux.getProximo();
				}
				if (aux.getProximo().equals(c)) {
					aux.setProximo(aux.getProximo().getProximo());
					inseridos--;
				}
			}
		}

	}

	/**
	 * Esse metodo ira remover um objeto pela posicao em que o mesmo esta inserido
	 * 
	 * @param pos
	 * @throws PosicaoInvalidaException
	 */

	public void removeByIndex(int pos) throws PosicaoInvalidaException {

		if (pos < 0 || pos > size()) {
			throw new PosicaoInvalidaException();
		}

		if (pos == 0) {
			primeiraConta = primeiraConta.getProximo();
			inseridos--;
		} else {

			Conta aux = primeiraConta;

			for (int i = 0; i < pos - 1; i++) {
				aux = aux.getProximo();
			}

			aux.setProximo(aux.getProximo().getProximo());
			inseridos--;
		}

	}

	/**
	 * Esse metodo ira remover o objeto que esta inserido na ultima posicao
	 * 
	 * @return
	 */

	public Conta removeLast() {

		Conta auxRetorno = null;

		if (primeiraConta.getProximo() == null) {
			auxRetorno = primeiraConta;
			primeiraConta = null;
			inseridos--;
			return auxRetorno;
		}

		Conta aux = primeiraConta;
		Conta auxAnterior = primeiraConta;

		while (aux.getProximo() != null) {
			auxAnterior = aux;
			aux = aux.getProximo();
		}

		auxRetorno = aux;
		auxAnterior.setProximo(null);
		inseridos--;

		return auxRetorno;

	}

	/**
	 * Este metodo serve para retornar o tamanho do deque encadeado
	 * 
	 * @return
	 */

	public int size() {
		return inseridos;
	}

	/**
	 * Este metodo serve para dizer se o deque encadeado esta vazio ou nao
	 * 
	 * @return
	 */

	public boolean isEmpty() {

		return inseridos == 0;
	}

	/**
	 * Metodo privado para comparar objetos e saber se existe dentro do deque
	 * encadeado
	 * 
	 * @param c
	 * @return
	 */


}